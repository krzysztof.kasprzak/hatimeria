<?php

namespace Feed;

/**
 * Class ProductFeedGenerator
 * @package Feed
 */
class ProductFeedGenerator
{
    const SYSTEM_1 = 'system1';
    const SYSTEM_2 = 'system2';

    /**
     * @var array
     */
    private $availableSystems = [
        self::SYSTEM_1 => ProductStorage::STORAGE_1,
        self::SYSTEM_2 => ProductStorage::STORAGE_2
    ];

    /**
     * @var string
     */
    private $system = self::SYSTEM_1;

    /**
     * @var null
     */
    private $storage = null;


    /**
     * @param $system
     * @param $format
     * @param array $skippFields
     * @return mixed
     */
    public function generateFeed($system, $format, $skippFields = [])
    {
        $products = $this->getProductsData($system);
        $formatter = new ProductFormatter($format);
        $formatter->setSkippFields($skippFields);
        $feed = $formatter->getFormattedData($products);
        return $feed;
    }


    /**
     * @param string $system
     */
    public function setSystem($system)
    {
        if (!array_key_exists($system, $this->availableSystems)) {
            throw new \InvalidArgumentException(
                "Given system: {$system} is not supported!"
            );
        }
        $this->system = $system;
    }

    /**
     * @return array
     */
    public function getAvailableSystems()
    {
        return $this->availableSystems;
    }

    /**
     * @return string
     */
    public function getSystem()
    {
        return $this->system;
    }

    /**
     *
     */
    private function setStorage()
    {
        $this->storage = new ProductStorage(
            $this->availableSystems[$this->system]
        );
    }

    /**
     * @param $system
     * @return mixed
     */
    private function getProductsData($system)
    {
        $this->setSystem($system);
        $this->setStorage();
        $products = $this->storage->findAll();
        return $products;
    }
}
