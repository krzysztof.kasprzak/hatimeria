<?php

namespace Feed;

use Feed\Formatter\FormatterFactoryMethod;
use Feed\Formatter\ProductFormatterFactoryMethod;
use Feed\Formatter\ProductFormatterInterface;

/**
 * Class ProductFormatter
 * @package Feed
 */
class ProductFormatter
{
    /**
     * @var null|ProductFormatterInterface
     */
    private $formatter = null;

    /**
     * ProductFormatter constructor.
     * @param $format
     */
    public function __construct($format = FormatterFactoryMethod::FORMAT_XML)
    {
        $factory = new ProductFormatterFactoryMethod();
        $formatter = $factory->create($format);
        $this->setFormatter($formatter);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getFormattedData($data)
    {
        return $this->formatter->getFormattedData($data);
    }


    /**
     * @param ProductFormatterInterface $formatter
     */
    private function setFormatter(ProductFormatterInterface $formatter)
    {
        $this->formatter = $formatter;
    }

    /**
     * @param array $fields
     */
    public function setSkippFields($fields)
    {
        $this->formatter->setSkippFields($fields);
    }
}
