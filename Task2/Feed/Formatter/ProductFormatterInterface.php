<?php

namespace Feed\Formatter;

/**
 * Interface ProductFormatterInterface
 * @package Feed\Formatter
 */
interface ProductFormatterInterface
{
    /**
     * @param $data
     * @return mixed
     */
    public function getFormattedData($data);

    /**
     * @param $fields
     * @return mixed
     */
    public function setSkippFields($fields);
}
