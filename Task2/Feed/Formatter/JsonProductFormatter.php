<?php

namespace Feed\Formatter;


use Feed\DataMapper\Product;

class JsonProductFormatter implements ProductFormatterInterface
{
    /**
     * @var array
     */
    private $itemStructure = [
        'title' => 'getName',
        'base' => [
            'price' => 'getPrice',
            'qty' => 'getStock',
            'canPurchase' => 'isAvailable',
        ],
        'attributes' => [
            'color' => 'getColor'
            ]
    ];

    /**
     * @var array
     */
    private $skippFields = [];

    /**
     * @param $data
     * @return string
     */
    public function getFormattedData($data)
    {
        return $this->generateJson($data);
    }

    /**
     * @param $data
     * @return string
     */
    private function generateJson($data)
    {
        $array = $this->addItems($data);
        $json = json_encode($array);
        return $json;
    }

    /**
     * @param $data
     * @return array
     */
    private function addItems($data)
    {
        $result = [];
        foreach ($data as $product) {
            $this->appendNode($result, '', $this->itemStructure, $product);
        }

        return $result;
    }

    private function appendNode(&$node, $nodeName, $item, Product $product)
    {
        if (is_array($item)) {

            $newNode = [];
            foreach ($item as $key => $value) {
                if (in_array($key, $this->skippFields)) {
                    continue;
                }
                $this->appendNode(
                    $newNode,
                    $key,
                    $value,
                    $product
                );
            }
            $node[$nodeName] = $newNode;
        } else {
            if (method_exists($product, $item)) {
                $node[$nodeName] = $product->$item();
            } else {
                throw new \Exception(
                    "Method {$item} not exists for given object"
                );
            }
        }
    }

    /**
     * @param array $skippFields
     */
    public function setSkippFields($skippFields)
    {
        if (!is_array($skippFields)) {
            throw new \InvalidArgumentException('Pass fields to skipp in array');
        }
        $this->skippFields = $skippFields;
    }
}
