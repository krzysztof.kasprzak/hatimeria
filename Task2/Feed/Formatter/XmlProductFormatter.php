<?php

namespace Feed\Formatter;

use Feed\DataMapper\Product;

/**
 * Class XmlProductFormatter
 * @package Feed\Formatter
 */
class XmlProductFormatter implements ProductFormatterInterface
{
    /**
     * @var array
     */
    private $itemStructure = [
        'item' => [
            'name' => 'getName',
            'purchasePrice' => 'getPrice',
            'stock' => [
                'stockQty' => [
                    'value' => 'getStock'
                ],
                'available' => [
                    'value' => 'isAvailable'
                ],
            ],
            'color' => 'getColor'
        ]
    ];

    /**
     * @var array
     */
    private $skippFields = [];

    /**
     * @var \DOMDocument
     */
    private $domDocument;

    /**
     * XmlProductFormatter constructor.
     */
    public function __construct()
    {
        $this->domDocument = new \DOMDocument('1.0', "UTF-8");
    }


    /**
     * @param $data
     * @return string
     */
    public function getFormattedData($data)
    {
        return $this->generateXml($data);
    }

    /**
     * @param $data
     * @return string
     */
    private function generateXml($data)
    {
        $this->appendRootNode($data);

        return $this->domDocument->saveXML();
    }

    /**
     * @param $data
     */
    private function appendRootNode($data)
    {
        $rootNode = $this->domDocument->createElement('feed');
        $this->appendItems($rootNode, $data);
        $this->domDocument->appendChild($rootNode);
    }

    /**
     * @param \DOMElement $parentNode
     * @param $data
     */
    private function appendItems(\DOMElement $parentNode, $data)
    {
        foreach ($data as $product) {
            $this->appendItem($parentNode, $product);
        }
    }

    /**
     * @param \DOMElement $parentNode
     * @param Product $product
     */
    private function appendItem(\DOMElement $parentNode, Product $product)
    {
        $this->appendNode($parentNode, 'item', $this->itemStructure, $product);

    }

    /**
     * @param \DOMElement $node
     * @param $nodeName
     * @param $item
     * @param Product $product
     * @return \DOMElement
     * @throws \Exception
     */
    private function appendNode(
        \DOMElement $node,
        $nodeName,
        $item,
        Product $product
    ) {
        if (is_array($item)) {
            foreach ($item as $key => $value) {
                if (in_array($key, $this->skippFields)) {
                    continue;
                }
                $newNode = $this->appendNode(
                    $this->domDocument->createElement($key),
                    $key,
                    $value,
                    $product
                );
                $node->appendChild($newNode);
            }
            $result = $node;
        } else {
            if (method_exists($product, $item)) {
                $newNode = $this->domDocument->createElement(
                    $nodeName,
                    $product->$item()
                );
                $result = $newNode;
            } else {
                throw new \Exception(
                    "Method {$item} not exists for given object"
                );
            }
        }

        return $result;
    }

    /**
     * @param $skippFields
     */
    public function setSkippFields($skippFields)
    {
        if (!is_array($skippFields)) {
            throw new \InvalidArgumentException('Pass fields to skipp in array');
        }
        $this->skippFields = $skippFields;
    }
}
