<?php

namespace Feed\Formatter;


class ProductFormatterFactoryMethod extends FormatterFactoryMethod
{
    /**
     * @param $format
     * @return ProductFormatterInterface
     */
    protected function createFormatter($format)
    {
        switch ($format) {
            case parent::FORMAT_JSON:
                $formatter = new JsonProductFormatter();
                break;
            case parent::FORMAT_XML:
                $formatter = new XmlProductFormatter();
                break;
            default:
                throw new \InvalidArgumentException(
                    "$format is not valid format"
                );
                break;
        }
        return $formatter;
    }
}
