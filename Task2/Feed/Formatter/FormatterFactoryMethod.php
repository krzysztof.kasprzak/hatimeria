<?php

namespace Feed\Formatter;


abstract class FormatterFactoryMethod
{
    /**
     * @var string
     */
    const FORMAT_XML = 'xml';

    /**
     * @var string
     */
    const FORMAT_JSON = 'json';

    abstract protected function createFormatter($format);

    public function create($format)
    {
        return $this->createFormatter($format);
    }
}
