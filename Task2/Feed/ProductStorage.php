<?php

namespace Feed;

use Feed\DataMapper\DefaultStorageAdapter;
use Feed\DataMapper\MapperInterface;
use Feed\DataMapper\ProductMapper;
use Feed\DataMapper\StorageAdapterInterface;

/**
 * Class Storage
 * @package Feed\DataMapper
 */
class ProductStorage
{

    const STORAGE_1 = 'storage1';
    const STORAGE_2 = 'storage2';
    const STORAGE_DEFAULT = 'default';

    /**
     * @var array
     */
    private $storages = [
        'default' => [
            'productA' => [
                'Availibility' => true,
                'Stock' => 233,
                'Price' => 120,
                'Color' => 'red'
            ]
        ],
        'storage1' => [
            'productA' => [
                'Availibility' => true,
                'Stock' => 233,
                'Price' => 130,
                'Color' => 'red',
                'ShippedIn' => '3days'
            ]
        ],
        'storage2' => [
            'productA' => [
                'Availibility' => true,
                'Stock' => 233,
                'Price' => 125,
                'Color' => 'red',
                'ShippedIn' => '7days'
            ]
        ]
    ];

    /**
     * @var null
     */
    private $mapper = null;

    /**
     * @var null
     */
    private $storageAdapter = null;

    /**
     * Storage constructor.
     * @param string $storage
     */
    public function __construct($storage = 'default')
    {
        $this->initStorage($storage);
    }

    /**
     * @param $storage
     */
    private function initStorage($storage)
    {
        switch ($storage) {
            case self::STORAGE_1:
                $this->setStorageAdapter(
                    new DefaultStorageAdapter($this->storages[$storage])
                );
                $this->setMapper(new ProductMapper($this->storageAdapter));
                break;
            case self::STORAGE_2:
                $this->setStorageAdapter(
                    new DefaultStorageAdapter($this->storages[$storage])
                );
                $this->setMapper(new ProductMapper($this->storageAdapter));
                break;
            case self::STORAGE_DEFAULT:
                $this->setStorageAdapter(
                    new DefaultStorageAdapter($this->storages[$storage])
                );
                $this->setMapper(new ProductMapper($this->storageAdapter));
                break;
            default:
                throw new \InvalidArgumentException(
                    "$storage is not valid storage"
                );
                break;
        }
    }

    /**
     * @param $name
     * @param $arguments
     * @return mixed
     * @throws \Exception
     */
    public function __call($name, $arguments)
    {
        if (!method_exists($this->mapper, $name)) {
            throw new \Exception("Method {$name} not exist for given storage!");
        }
        return call_user_func_array([$this->mapper, $name], $arguments);
    }

    /**
     * @param StorageAdapterInterface $storageAdapter
     */
    private function setStorageAdapter(StorageAdapterInterface $storageAdapter)
    {
        $this->storageAdapter = $storageAdapter;
    }

    /**
     * @param MapperInterface $mapper
     */
    private function setMapper(MapperInterface $mapper)
    {
        $this->mapper = $mapper;
    }
}
