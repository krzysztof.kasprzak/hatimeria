<?php

namespace Feed\DataMapper;

/**
 * Class StorageAdapter
 * @package Feed\DataMapper
 */
class DefaultStorageAdapter implements StorageAdapterInterface
{
    /**
     * @var array
     */
    private $data = [];

    /**
     * BaseAdapter constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * @param $id
     *
     * @return array|null
     */
    public function find($id)
    {
        if (isset($this->data[$id])) {
            return $this->data[$id];
        }

        return null;
    }

    /**
     * @return array
     */
    public function findAll()
    {
        return $this->data;
    }
}
