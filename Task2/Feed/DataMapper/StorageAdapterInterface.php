<?php

namespace Feed\DataMapper;

/**
 * Interface StorageAdapterInterface
 * @package Feed\DataMapper
 */
interface StorageAdapterInterface
{
    /**
     * StorageAdapterInterface constructor.
     * @param array $data
     */
    public function __construct(array $data);

    /**
     * @param $id
     * @return mixed
     */
    public function find($id);

    /**
     * @return mixed
     */
    public function findAll();
}
