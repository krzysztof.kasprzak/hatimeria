<?php

namespace Feed\DataMapper;

/**
 * Class ProductMapper
 * @package Feed\DataMapper
 */
class ProductMapper implements MapperInterface
{
    /**
     * @var StorageAdapterInterface
     */
    private $adapter;

    /**
     * ProductMapper constructor.
     * @param StorageAdapterInterface $storage
     */
    public function __construct(StorageAdapterInterface $storage)
    {
        $this->adapter = $storage;
    }

    /**
     * @param $id
     *
     * @return Product
     */
    public function findById($id)
    {
        $result = $this->adapter->find($id);

        if ($result === null) {
            throw new \InvalidArgumentException("Product #$id not found");
        }
        $result['Id'] = $id;
        return $this->mapRowToUser($result);
    }

    /**
     * @return array|null
     */
    public function findAll()
    {
        $result = null;
        $products = $this->adapter->findAll();
        foreach ($products as $id => $product) {
            $product['Id'] = $id;
            $result[] = $this->mapRowToUser($product);
        }
        return $result;
    }

    /**
     * @param array $row
     * @return Product
     */
    private function mapRowToUser(array $row)
    {
        $map = [
            'Id' => 'name',
            'Availibility' => 'availability',
            'Stock' => 'stock',
            'Price' => 'price',
            'Color' => 'color',
            'ShippedIn' => 'shipped_in'
        ];
        $data = [];
        foreach ($row as $key => $field) {
            if (isset($map[$key])) {
                $data[$map[$key]] = $field;
            }
        }

        return Product::fromState($data);
    }
}