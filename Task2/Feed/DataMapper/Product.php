<?php

namespace Feed\DataMapper;


/**
 * Class Product
 * @package Feed\DataMapper
 */
class Product
{
    /**
     * @var string
     */
    private $name = '';

    /**
     * @var bool
     */
    private $availability = false;

    /**
     * @var int
     */
    private $stock = 0;

    /**
     * @var int
     */
    private $price = 0;

    /**
     * @var string
     */
    private $color = '';

    /**
     * @var string
     */
    private $shippedIn = '';

    /**
     * @param array $state
     * @return Product
     */
    public static function fromState(array $state)
    {
        $requiredKeys = [
            'name' => true,
            'availability' => true,
            'stock' => true,
            'price' => true,
            'color' => true,
            'shipped_in' => false
        ];

        foreach ($requiredKeys as $key => $required) {

            if (!isset($state[$key])) {
                if ($required) {
                    throw new \InvalidArgumentException(
                        "Given data is incomplete, missing field: {$key}"
                    );
                } else {
                    $state[$key] = null;
                }
            }
        }

        return new self(
            $state['name'],
            $state['availability'],
            $state['stock'],
            $state['price'],
            $state['color'],
            $state['shipped_in']
        );
    }


    /**
     * Product constructor.
     * @param $name
     * @param $availability
     * @param $stock
     * @param $price
     * @param $color
     * @param $shippedIn
     */
    public function __construct(
        $name,
        $availability,
        $stock,
        $price,
        $color,
        $shippedIn
    ) {
        $this->name = $name;
        $this->availability = $availability;
        $this->stock = $stock;
        $this->price = $price;
        $this->color = $color;
        $this->shippedIn = $shippedIn;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return boolean
     */
    public function getAvailability()
    {
        return $this->availability;
    }

    /**
     * @param boolean $availability
     */
    public function setAvailability(bool $availability)
    {
        $this->availability = $availability;
    }


    /**
     * @return boolean
     */
    public function isAvailable()
    {
        return $this->availability;
    }

    /**
     * @return int
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @return string
     */
    public function getShippedIn()
    {
        return $this->shippedIn;
    }
}
