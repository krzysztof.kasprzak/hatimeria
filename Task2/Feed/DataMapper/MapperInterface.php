<?php

namespace Feed\DataMapper;


/**
 * Interface MapperInterface
 * @package Feed\DataMapper
 */
interface MapperInterface
{
    /**
     * @return mixed
     */
    public function findAll();

    /**
     * @param $id
     * @return mixed
     */
    public function findById($id);
}
