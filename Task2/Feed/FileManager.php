<?php

namespace Feed;

/**
 * Class FileManager
 * @package Feed
 */
class FileManager
{
    /**
     * @param $data
     * @param $fileName
     * @return int
     * @throws \Exception
     */
    public function saveDataToFile($data, $fileName)
    {
        $filePath = $this->getSaveDirectoryPath() . $fileName;

        if (!$handle = fopen($filePath, 'w')) {
            throw new \Exception('Cannot save to file:  ' . $filePath);
        }
        return fwrite($handle, $data);
    }

    /**
     * @param $dirPath
     * @throws \Exception
     */
    public function createDirectoryIdNotExists($dirPath)
    {
        if (!file_exists($dirPath)) {
            if (!mkdir($dirPath, 0777, true)) {
                throw new \Exception("Can not create directory: $dirPath");
            }
        }
    }

    /**
     * @return string
     */
    private function getSaveDirectoryPath()
    {
        $dir = dirname(__FILE__) . '/../files/';
        $this->createDirectoryIdNotExists($dir);

        return $dir;
    }
}
