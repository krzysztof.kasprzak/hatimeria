<?php
require_once dirname(__FILE__) . '/vendor/autoload.php';

$fm = new \Feed\FileManager();
$feedGenerator = new \Feed\ProductFeedGenerator();
$feed = $feedGenerator->generateFeed('system1', 'json', ['base']);

foreach (['json', 'xml'] as $format) {
    foreach (['system1', 'system2'] as $system) {
        try {
            $feed = $feedGenerator->generateFeed($system, $format);
            $fm->saveDataToFile($feed, "$system.export.$format");
        } catch (\Exception $exception) {
            echo "Feed $system/$format not generated, {$exception->getMessage()}";
        }
    }
}
