<?php
require dirname(__FILE__) . '/ExampleCode/Example.php';
require dirname(__FILE__) . '/ExampleCode/ExampleIterator.php';

use ExampleCode\Example;
use ExampleCode\ExampleIterator;

$object = new Example();
$object->setItems(array(
    'element',
    'element1',
    'element2',
    'element3'
));
$object->setIterator(new ExampleIterator());

foreach ($object as $element) {
    var_dump($element);
}
