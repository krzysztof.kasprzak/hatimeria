<?php

namespace ExampleCode;

/**
 * Class Example
 */
class Example implements \IteratorAggregate
{

    /**
     * @var array
     */
    private $items = [];

    /**
     * @var null
     */
    private $iterator = null;

    /**
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param array $items
     */
    public function setItems(array $items)
    {
        $this->items = $items;
    }

    /**
     * @return mixed
     */
    public function getIterator()
    {
        if (null === $this->iterator) {
            $this->setDefaultIterator();
        }
        $className = get_class($this->iterator);
        $this->iterator = new $className($this->items);

        return $this->iterator;
    }

    /**
     * @param \Iterator $iterator
     */
    public function setIterator(\Iterator $iterator)
    {
        $this->iterator = $iterator;
        $this->iterator = $this->getIterator();
    }


    /**
     * @return void
     */
    public function setDefaultIterator()
    {
        $this->iterator = new \ArrayIterator($this->items);
    }
}
