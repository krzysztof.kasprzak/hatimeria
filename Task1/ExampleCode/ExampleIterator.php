<?php
namespace ExampleCode;

/**
 * Class ExampleIterator
 * @package ExampleCode
 */
class ExampleIterator implements \Iterator
{
    /**
     * @var array
     */
    private $items = [];

    /**
     * @var null
     */
    private $position = null;

    /**
     * ExampleIterator constructor.
     * @param array $items
     */
    public function __construct(array $items = [])
    {
        $this->setItems($items);
    }

    /**
     * @param array $items
     */
    private function setItems(array $items)
    {
        $this->items = $items;

        foreach ($this->items as $key => $item) {
            if (!$this->isEven($key)) {
                unset($this->items[$key]);
            }
        }

        $this->rewind();
    }

    /**
     * @return mixed|null
     */
    public function current()
    {
        $result = null;
        if (isset($this->items[$this->position])) {
            $result = $this->items[$this->position];
        }
        return $result;
    }

    /**
     * @return void
     */
    public function next()
    {
        $result = null;
        $lastKey = $this->getLastArrayKey();

        if ($this->position !== $lastKey) {

            $currentPassed = false;
            foreach ($this->items as $key => $item) {
                if ($currentPassed) {
                    $result = $key;
                    break;
                }
                if ($this->position === $key) {
                    $currentPassed = true;
                }
            }
        }
        $this->position = $result;
    }

    /**
     * @return null
     */
    public function key()
    {
        return $this->position;
    }

    /**
     * @return bool
     */
    public function valid()
    {
        return isset($this->items[$this->position]);
    }

    /**
     * @return void
     */
    public function rewind()
    {
        if (!empty($this->items)) {
            reset($this->items);
            $this->position = key($this->items);
        }
    }

    /**
     * @param $value
     * @return bool
     */
    private function isEven($value)
    {
        return is_integer($value) ? 0 === $value % 2 : false;
    }

    /**
     * @return mixed
     */
    private function getLastArrayKey()
    {
        end($this->items);
        $lastKey = key($this->items);
        return $lastKey;
    }
}
